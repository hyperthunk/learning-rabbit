-module(process).

-export([start/0]).

start() ->
    spawn(fun () -> f() end).

f() ->
    receive
        i_never_expect_to_get_this -> ok;
        Other -> io:format("Unexpected input: ~p~n", [Other]), f()
    end.
