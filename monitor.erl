-module(monitor).

-export([start/1, test/1]).

test(Msg) ->
    ?MODULE ! {send, Msg}.

start(Node) ->
    register(?MODULE, spawn(fun() -> init(Node) end)).

init(Node) ->
    net_kernel:monitor_nodes(true),
    Pid = rpc:call(Node, process, start, []),
    erlang:monitor(process, Pid),
    StoredPid = term_to_binary(Pid),
    io:format("Started process:~n", []),
    io:format("~p~n", [StoredPid]),
    put(remote_pid, StoredPid),
    recv_loop().

recv_loop() ->
    receive
        {send, Msg} -> Pid = binary_to_term(get(remote_pid)),
                       io:format("Sending ~p to ~p~n", [Msg, Pid]),
                       Pid ! Msg,
                       recv_loop();
        X           -> io:format("Received ~p~n", [X]),
                       recv_loop()
    end.
